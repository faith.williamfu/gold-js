/**
 * This function turns the specified string into camel cased identifier. Camel case (stylized
 * as camelCase; also known as camel caps or more formally as medial capitals) is the practice
 * of writing phrases such that each word or abbreviation in the middle of the phrase begins
 * with a capital letter, with no intervening spaces or punctuation. For example.
 *
 * - `safe hTML` -> `safeHtml`
 * - `escape HTML entities` -> `escapeHtmlEntities`
 *
 * The identifier should only contains english letters (`A` to `Z`, including upper and
 * lower case), digits (`0` to `9`) and underscore (`_`). Other characters will be treated
 * as delimiters. For example.
 *
 * - `safe+html` -> `safeHtml`
 *
 * @param {String} string The input string.
 */
export default function camelCase (string) {
  // TODO:
  //   Please implement the function.
  // <-start-
  if (string === undefined) {
    return undefined;
  } else if (string == null) {
    return null;
  }

  const StringArray = getStringArray(string);
  let camelString = null;
  for (let i = 0; i < StringArray.length; i++) {
    camelString = StringArray.join('');
  }
  return camelString;
  // --end-->
}

// TODO:
//   You can add additional code here if you want
// <-start-
function fisrtLetterUpperCase (char) {
  const newChar = char[0].toUpperCase() + char.substring(1, char.length);
  return newChar;
}

function getStringArray (string) {
  const StringArray = string.replace(/[^a-zA-Z0-9_]/ig, ' ').split(' ');
  for (let i = 0; i < StringArray.length; i++) {
    StringArray[i] = StringArray[i].toLowerCase();
    StringArray[i] = fisrtLetterUpperCase(StringArray[i]);
  }
  StringArray[0] = StringArray[0].toLowerCase();
  return StringArray;
}
// --end-->
